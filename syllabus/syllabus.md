# Syllabus for the Linear Algebra course for 19/20


## Degrees I'm teaching in:
1. Industrial engineering
2. Industrial engineering (Spanish)
3. Data Science engineering
4. Telecommunications engineering


## Common contents
1. Complex numbers
    - Binomial and polar forms
    - Root of complex numbers
    - Equations with complex numbers
2. Systems of equations
    - Linear systems
    - Matrices associated with linear systems
    - Row reduction algorithm
    - Linear systems as vector equations
    - Linear systems as matrix equations
3. Rn / Cn space
    - Homogeneous systems
    - Linear independence
4. Eigenvalues and eigenvectors
5. Matrix algebra
6. Ortogonality
7. Normal and symmetric matrices
8. Least-squares
9. Differential equations
10. Singular value decomposition

