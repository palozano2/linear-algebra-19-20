{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need the following packages:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import scipy.linalg as la"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Linear systems"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A linear system of equations is a collection of linear equations, such as:\n",
    "$$ a_{0,0}x_0 + a_{0,1}x_1 + a_{0,n}x_n = b_0, \\\\ a_{1,0}x_0 + a_{1,1}x_1 + a_{1,n}x_n = b_1, \\\\ \\vdots\\\\ a_{0,0}x_0 + a_{0,1}x_1 + a_{0,m}x_m = b_m. $$\n",
    "\n",
    "It is possible to write a linear system in many ways.\n",
    "\n",
    "In matrix notation, the previous linear system would be $A\\textbf{x}=\\textbf{b},$ where\n",
    "$$ A\\textbf{x} = \\begin{bmatrix} a_{0,0} & a_{0,1} & \\cdots & a_{0,n} \\\\ a_{1,0} & a_{1,1} & \\cdots & a_{1,n} \\\\ \\vdots & \\vdots & \\ddots & \\vdots \\\\ a_{m,0} & a_{m,1} & \\cdots & a_{m,n} \\end{bmatrix} \\begin{bmatrix} x_0 \\\\ x_1 \\\\ \\vdots \\\\ x_n \\end{bmatrix} = \\begin{bmatrix} b_0 \\\\ b_1 \\\\ \\vdots \\\\ b_n \\end{bmatrix} = \\textbf{b}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Elementary row operations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Elementary Row Operations, or ERO for short, include:\n",
    "1. Add $k$ times row $j$ to row $i$.\n",
    "2. Multiply row $i$ by scalar $k$.\n",
    "3. Switch rows $i$ and $j$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Imagine we have the following matrix $A:$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A = \n",
      "[[ 1  1  2]\n",
      " [-1  3  1]\n",
      " [ 0  5  2]]\n"
     ]
    }
   ],
   "source": [
    "A = np.array([[1,1,2],[-1,3,1],[0,5,2]])\n",
    "print('A = \\n{}'.format(A))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And we want to use the row-reduction algorithm to get a zero in the first position of the first column. We would need to add the first row to the second one, i.e., $R_2'R_2+R_1.$ (Remember that, in Pytohn, indexing starts at 0.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A = \n",
      "[[1 1 2]\n",
      " [0 4 3]\n",
      " [0 5 2]]\n"
     ]
    }
   ],
   "source": [
    "A[1] = A[1] + A[0]\n",
    "print('A = \\n{}'.format(A))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We could keep reducing the matrix until we have the echelon form as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A = \n",
      "[[ 1  1  2]\n",
      " [ 0  4  3]\n",
      " [ 0  0 -1]]\n"
     ]
    }
   ],
   "source": [
    "A[2] = A[2] - A[1]*(5/4)\n",
    "print('A = \\n{}'.format(A))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Another, more efficient, way of reducing matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Each of the elementary row operations is the result of matrix multiplication (on the left) by an elementary matrix. To add $k$ times row $i$ to row $j$ in a matrix $A,$ we multiply $A$ by the identity matrix with $k$ in the $(i,j)$ position. For example \n",
    "\n",
    "$$ E = \\begin{bmatrix} 1 & 0 & 3 \\\\ 0 & 1 & 0 \\\\ 0 & 0 & 1 \\end{bmatrix} $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A = \n",
      "[[ 1  1  2]\n",
      " [-1  3  1]\n",
      " [ 0  5  2]]\n",
      "\n",
      "E * A = \n",
      "[[ 1 16  8]\n",
      " [-1  3  1]\n",
      " [ 0  5  2]]\n"
     ]
    }
   ],
   "source": [
    "A = np.array([[1,1,2],[-1,3,1],[0,5,2]])\n",
    "E1 = np.array([[1,0,3],[0,1,0],[0,0,1]])\n",
    "print('A = \\n{}'.format(A))\n",
    "\n",
    "tmp = E1 @ A\n",
    "print('\\nE * A = \\n{}'.format(tmp))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To multiply row $i$ $k$ times in a matrix $A,$ we multiply $A$ by the identity matrix with $k$ in the $(i,i)$ position. For example \n",
    "\n",
    "$$ E = \\begin{bmatrix} 1 & 0 & 0 \\\\ 0 & -2 & 0 \\\\ 0 & 0 & 1 \\end{bmatrix} $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 1,  1,  2],\n",
       "       [ 2, -6, -2],\n",
       "       [ 0,  5,  2]])"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "E2 = np.array([[1,0,0],[0,-2,0],[0,0,1]])\n",
    "E2 @ A"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, to switch row $i$ and row $j$ in a matrix $A$, we multiply $A$ by the identity matrix where $E_{i,i}=0$, $E_{j,j}=0$, $E_{i,j}=1$ and $E_{j,i}=1$. For example,\n",
    "\n",
    "$$ E = \\begin{bmatrix} 1 & 0 & 0 \\\\ 0 & 0 & 1 \\\\ 0 & 1 & 0 \\end{bmatrix} $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 1,  1,  2],\n",
       "       [ 0,  5,  2],\n",
       "       [-1,  3,  1]])"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "E3 = np.array([[1,0,0],[0,0,1],[0,1,0]])\n",
    "E3 @ A"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To practice a bit of programming in Python and these concepts, try to implement the previous three elementary operations as functions. One possible implementation is given below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add_row(A,k,i,j):\n",
    "    \"Add k times row j to row i in matrix A.\"\n",
    "    n = A.shape[0]\n",
    "    E = np.eye(n)\n",
    "    if i == j:\n",
    "        E[i,i] = k + 1\n",
    "    else:\n",
    "        E[i,j] = k\n",
    "    return E @ A\n",
    "\n",
    "def scale_row(A,k,i):\n",
    "    \"Multiply row i by k in matrix A.\"\n",
    "    n = A.shape[0]\n",
    "    E = np.eye(n)\n",
    "    E[i,i] = k\n",
    "    return E @ A\n",
    "\n",
    "def switch_rows(A,i,j):\n",
    "    \"Switch rows i and j in matrix A.\"\n",
    "    n = A.shape[0]\n",
    "    E = np.eye(n)\n",
    "    E[i,i] = 0\n",
    "    E[j,j] = 0\n",
    "    E[i,j] = 1\n",
    "    E[j,i] = 1\n",
    "    return E @ A"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We're going to use our new fuctions to solve the linear system $Ax=b.$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "A = \n",
      "[[ 6 15  1]\n",
      " [ 8  7 12]\n",
      " [ 2  7  8]]\n",
      "b = \n",
      "[[ 2]\n",
      " [14]\n",
      " [10]]\n"
     ]
    }
   ],
   "source": [
    "A = np.array([[6,15,1],[8,7,12],[2,7,8]])\n",
    "print('A = \\n{}'.format(A))\n",
    "b = np.array([[2],[14],[10]])\n",
    "print('b = \\n{}'.format(b))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We form the augmented matrix $M.$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 6 15  1  2]\n",
      " [ 8  7 12 14]\n",
      " [ 2  7  8 10]]\n"
     ]
    }
   ],
   "source": [
    "M = np.hstack([A,b])\n",
    "print(M)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And then perform the desired operations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 1.          2.5         0.16666667  0.33333333]\n",
      " [ 8.          7.         12.         14.        ]\n",
      " [ 2.          7.          8.         10.        ]]\n",
      "[[  1.           2.5          0.16666667   0.33333333]\n",
      " [  0.         -13.          10.66666667  11.33333333]\n",
      " [  2.           7.           8.          10.        ]]\n",
      "[[  1.           2.5          0.16666667   0.33333333]\n",
      " [  0.         -13.          10.66666667  11.33333333]\n",
      " [  0.           2.           7.66666667   9.33333333]]\n",
      "[[ 1.          2.5         0.16666667  0.33333333]\n",
      " [ 0.          1.         -0.82051282 -0.87179487]\n",
      " [ 0.          2.          7.66666667  9.33333333]]\n",
      "[[ 1.          2.5         0.16666667  0.33333333]\n",
      " [ 0.          1.         -0.82051282 -0.87179487]\n",
      " [ 0.          0.          9.30769231 11.07692308]]\n",
      "[[ 1.          2.5         0.16666667  0.33333333]\n",
      " [ 0.          1.         -0.82051282 -0.87179487]\n",
      " [ 0.          0.          1.          1.19008264]]\n",
      "[[1.         2.5        0.16666667 0.33333333]\n",
      " [0.         1.         0.         0.1046832 ]\n",
      " [0.         0.         1.         1.19008264]]\n",
      "[[1.         2.5        0.         0.13498623]\n",
      " [0.         1.         0.         0.1046832 ]\n",
      " [0.         0.         1.         1.19008264]]\n",
      "[[ 1.          0.          0.         -0.12672176]\n",
      " [ 0.          1.          0.          0.1046832 ]\n",
      " [ 0.          0.          1.          1.19008264]]\n"
     ]
    }
   ],
   "source": [
    "M1 = scale_row(M,1/6,0)\n",
    "print(M1)\n",
    "\n",
    "M2 = add_row(M1,-8,1,0)\n",
    "print(M2)\n",
    "\n",
    "M3 = add_row(M2,-2,2,0)\n",
    "print(M3)\n",
    "\n",
    "M4 = scale_row(M3,-1/13,1)\n",
    "print(M4)\n",
    "\n",
    "M5 = add_row(M4,-2,2,1)\n",
    "print(M5)\n",
    "\n",
    "M6 = scale_row(M5,1/M5[2,2],2)\n",
    "print(M6)\n",
    "\n",
    "M7 = add_row(M6,-M6[1,2],1,2)\n",
    "print(M7)\n",
    "\n",
    "M8 = add_row(M7,-M7[0,2],0,2)\n",
    "print(M8)\n",
    "\n",
    "M9 = add_row(M8,-M8[0,1],0,1)\n",
    "print(M9)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have now our solution:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[-0.12672176]\n",
      " [ 0.1046832 ]\n",
      " [ 1.19008264]]\n"
     ]
    }
   ],
   "source": [
    "x = M9[:,3].reshape(3,1)\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is fine to learn programming, how functions work, and how to manage data.\n",
    "But when we learn how the Gauss algortithm works, we can just use one of the (more efficient) functions implemented in some package, like SciPy.\n",
    "For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[-0.12672176]\n",
      " [ 0.1046832 ]\n",
      " [ 1.19008264]]\n"
     ]
    }
   ],
   "source": [
    "x = la.solve(A,b)\n",
    "print(x)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
